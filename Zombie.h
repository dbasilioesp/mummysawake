#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include "engcomp_glib.h"
#include <ctime>
#include <stdlib.h>

using namespace std;

class Zombie
{
public:
    Zombie(void);
    ~Zombie(void);
    
    void initialize(int tipo, float x_, float y_);
    
    void walk();
    void draw();
    
    float getX();
    float getY();
    
    int getWidth();
    int getHeight();
    
    imagem getSprite();
    
    void setX(float x);
    void setY(float y);
    void setHealth(int health);
    void setSpeed(float speed);
    void die();
	void takeDamage();
	bool isRIP();
    
protected:
    imagem sprite[3];
    int health, zombieType;
    float speed, x, y;
    bool isDead;
};

