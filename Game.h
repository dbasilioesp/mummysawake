#pragma once
#include "Zombie.h"
#include "Grave.h"

class Game
{
public:
    Game(void);
    ~Game(void);
    
    void initialize();
    void play();
    void finalize();
    
	void createScene(int nGraves_, int scene_);
	void run();
	void menu();
	void credits();
	void gameOver();
	void tutorial();

    void createGraves();
    void createZombies();
    
	void draw();
    void drawGameOver();
	void drawMenu();
	void drawCredits();
	void drawTutorial();
	void drawCountFont();

	void killZombies();
	void destroyGrave();

	void nextWave();

protected:
    imagem background, hammer, backgroundGameOver, backgroundMenu, backgroundCredits, backgroundTutorial;
    bool grid[8][4], isGameOver, hasClicked, exit, isPlayingMusic, stoppedGame;
    Grave graves[42];
    Zombie zombies[100];
    int nWaves, nGraves, nZombies, whichGrave, zombiesWhacked, gravesCracked, goZombie, randomZombie, scene, time, randZombies, btID;
    Interface guiMenu, guiCredits, guiGameOver, guiPlaying, guiPaused, guiTutorial;
	musica ingame_sound, menu_sound;
	som bite;
	fonte countFont;
};

