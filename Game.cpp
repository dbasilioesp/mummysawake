#include "Game.h"

static bool pressionado = false;

bool egl_clique_mouse(EGL_EVENTOS botao)
{
	if( ( mouse_b & SDL_BUTTON(botao)) && (!pressionado))
	{
		pressionado = true;
		return true;
	}        
	else if (!mouse_b)
	{
		pressionado = false;
		return false;
	}
	return false;
}

Game::Game(void)
{
	egl_inicializar(800,600,true);
}

Game::~Game(void){}

void Game::initialize()
{
	backgroundTutorial.carregar(".\\backgrounds\\TutorialPage.png");
	backgroundMenu.carregar(".\\backgrounds\\menu_Anubis_Oficial_2.png");
	backgroundCredits.carregar(".\\backgrounds\\credits_background.png");
	background.carregar(".\\backgrounds\\background_game.png");
	backgroundGameOver.carregar(".\\backgrounds\\GameOverNew.png");
	hammer.carregar(".\\sprites\\cetro1.png");
	//menu_sound.carregar(".\\sounds\\menu.mp3");
	ingame_sound.carregar(".\\sounds\\game.mp3");
	//bite.carregar(".\\sounds\\bite.mp3");

	countFont.carregar(".\\fontes\\Papyrus.ttf", 40);
	
	guiMenu.adicionaComponente(new Botao(0,595,140, ".\\buttons\\play1.png",".\\buttons\\play2.png",".\\buttons\\play2.png"));
	guiMenu.adicionaComponente(new Botao(1,595,235, ".\\buttons\\tutorial1.png",".\\buttons\\tutorial2.png",".\\buttons\\tutorial2.png"));
	guiMenu.adicionaComponente(new Botao(2,595,329, ".\\buttons\\credits1.png",".\\buttons\\credits2.png",".\\buttons\\credits2.png"));
	guiMenu.adicionaComponente(new Botao(3,595,424, ".\\buttons\\exit1.png",".\\buttons\\exit2.png",".\\buttons\\exit2.png"));

	ItemMenu* item_exit1 = new ItemMenu(0,"Exit", 650, 13, 100, 70);
	item_exit1->setarFonte(".\\fontes\\Papyrus.ttf", 40, false);

	guiPlaying.adicionaComponente(item_exit1);

	ItemMenu* item_paused = new ItemMenu(0,"Continue", 340, 370, 200, 35);
	item_paused->setarFonte(".\\fontes\\Papyrus.ttf",32,false);

	ItemMenu* item_exit2 = new ItemMenu(1,"Exit", 340, 410, 200, 35);
	item_exit2->setarFonte(".\\fontes\\Papyrus.ttf",32,false);

	guiPaused.adicionaComponente(item_paused);
	guiPaused.adicionaComponente(item_exit2);
	
	guiCredits.adicionaComponente(new Botao(0,315,435,".\\buttons\\back.png",".\\buttons\\back.png",".\\buttons\\back.png"));
	guiTutorial.adicionaComponente(new Botao(0,315,475,".\\buttons\\back.png",".\\buttons\\back.png",".\\buttons\\back.png"));

	ItemMenu* item_playagain = new ItemMenu(0,"Play Again", 340, 470, 200, 35);
	item_playagain->setarFonte(".\\fontes\\Papyrus.ttf",32,false);
	
	ItemMenu* item_exit3 = new ItemMenu(1,"Exit", 340, 520, 200, 35);
	item_exit3->setarFonte(".\\fontes\\Papyrus.ttf",32,false);

	guiGameOver.adicionaComponente(item_playagain);
	guiGameOver.adicionaComponente(item_exit3);

	exit = false;
	isPlayingMusic = false;
	isGameOver = false;
	stoppedGame = false;
	nWaves = 0;
	btID = 0;
	
}

void Game::createScene(int nGraves_, int scene_)
{
	scene = scene_;
	nGraves = nGraves_;
	nZombies = 0;
	zombiesWhacked = 0;
	gravesCracked = 0;
	time = 0;
	hasClicked = false;
	isGameOver = false;
	stoppedGame = false;
	randZombies = 400;
	
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			grid[i][j] = false;
		}
	}

	createGraves();

	ingame_sound.tocar();
	isPlayingMusic = true;
}

void Game::play()
{
	scene = 0;
	
	while(!exit)
	{

		switch(scene)
		{
			case 0:
				menu();
				break;
			case 1:
				// Preparar game
				createScene(3, 2);// 3 tumbas e 2 � sei
			case 2:
				run();
				break;
			case 3:
				credits();
				break;
			case 4:
				gameOver();
				break;
			case 5:
				tutorial();
				break;
		}

		
		egl_desenha_frame();
	}
    
}

// SCENES
// ****************************************************************

void Game::gameOver()
{

	btID = guiGameOver.verificaEventos(EGL_CLIQUE_ESQ);
	nWaves = 0;
	switch(btID)
	{
		case 0: scene = 1; 
				break;
		case 1: scene = 0;
				break;
	}

	drawGameOver();
}

void Game::run()
{
	if(gravesCracked == nGraves)
	{
		nextWave();
	}

	if(nWaves > 2){
		isGameOver = true;
	}

	if(key[SDLK_ESCAPE])
	{
		stoppedGame = true;
	}

	btID = guiPlaying.verificaEventos(EGL_CLIQUE_ESQ);
	if(btID == 0)
	{
		scene = 0;
	}

	if(!isGameOver && !stoppedGame)
	{
            
		if (time % 200 == 0)
		{
			createZombies();
		}

		for(int i = 0; i < nZombies; i++)
		{
			zombies[i].walk();
                
			if(zombies[i].getX() < 0)
			{
				isGameOver = true;
			}
		}
        
		if(egl_clique_mouse(EGL_CLIQUE_ESQ))
		{
			//hasClicked = true;
            killZombies();
		}

		draw();
		time++;
			
	}
        
	if(isGameOver)
	{
		scene = 4;
	}

	if(stoppedGame)
	{
		btID = guiPaused.verificaEventos(EGL_CLIQUE_ESQ);

		switch(btID)
		{
			case 0:
				stoppedGame = false;
				break;
			case 1:
				scene = 0;
				ingame_sound.parar();
				break;
		}

		draw();
		guiPaused.desenha();
	}

}

void Game::menu()
{

	if(!isPlayingMusic){
		//menu_sound.tocar();
		isPlayingMusic = true;
	}

	btID = guiMenu.verificaEventos(EGL_CLIQUE_ESQ);

	switch(btID)
	{
		case 0: scene = 1; break;
		case 1: scene = 5; break;
		case 2: scene = 3; break;
		case 3: exit = true; break;
		
	}

	if(key[SDLK_ESCAPE]){
		exit = true;
	}

	drawMenu();
}

void Game::credits()
{
	btID = guiCredits.verificaEventos(EGL_CLIQUE_ESQ);
	if(btID == 0)
	{
		scene = 0;
	}

	drawCredits();
}


void Game::tutorial()
{
	int btID = guiTutorial.verificaEventos(EGL_CLIQUE_ESQ);
	if(btID == 0)
	{
		scene = 0;
	}

	backgroundTutorial.desenha(0, 0);
	guiTutorial.desenha();
	SDL_ShowCursor(SDL_ENABLE);
	
}

void Game::createGraves()
{
	int i = 0;
	int squareX, squareY;
	int centerX = 20, centerY = 200;
    
	while(i < nGraves)
	{
		squareX = (rand() % 3) + 5;
		squareY = rand() % 4;
        
		// Criar a grave caso o espa�o n�o esteja ocupado
		if (grid[squareX][squareY] == false)
		{
			grid[squareX][squareY] = true; // essa posi��o agora est� ocupada
			graves[i].initialize(squareX * 100 + centerX, squareY * 100 + centerY); // 520, 310
			i++;
		}
        
	}
    
}

void Game::createZombies()
{
	goZombie = rand() % randZombies;
    
	if (goZombie < 150)
	{

		int zombieType;
		randomZombie = rand () % 1000;
		
		if(randomZombie < 500)
		{
			zombieType = 0;
		}
		else if(randomZombie < 800)
		{
			zombieType = 1;
		}
		else if(randomZombie < 1000)
		{
			zombieType = 2;
		}
		// Seleciona uma tumba qualquer
		
		bool selectedGrave = false;

		while(!selectedGrave)
		{
			whichGrave = rand() % nGraves;
			if(!graves[whichGrave].isDestroyed())
			{
				selectedGrave = true;
			}
		}
		zombies[nZombies].initialize(zombieType, graves[whichGrave].getX() - 30, graves[whichGrave].getY() + 25);
		nZombies++;
	}
    
}

void Game::destroyGrave()
{
	bool notDestroyed = true;
	while(notDestroyed){
		whichGrave = rand() % nGraves;

		if(!graves[whichGrave].isDestroyed()){
			graves[whichGrave].destroy();
			gravesCracked++;
			zombiesWhacked = 0;
			notDestroyed = false;

			if(randZombies > 300)
				randZombies -= 100;
		}
	}

	
}

void Game::nextWave()
{
	nWaves++;
	createScene(nGraves + nWaves, 2);
}

void Game::killZombies()
{
	for(int i = 0; i < nZombies; i++)
	{
		if(!zombies[i].isRIP())
		{
			if(hammer.colide(mouse_x - 10, mouse_y, hammer.getResX() - 25, hammer.getResY() - 50 ,zombies[i].getX(), zombies[i].getY(), zombies[i].getWidth(), zombies[i].getHeight(), zombies[i].getSprite()))
			{
				zombies[i].takeDamage();
				//bite.tocar();

				// Aqui ele acertou, ye
				if(zombies[i].isRIP())
				{
					zombiesWhacked++;	
				}
				
				if(zombiesWhacked == 10)
				{
					destroyGrave();
				}

				// Hit just one zombie by time
				break;
			}
		}
					
					
	}
}

// DRAWS
// ****************************************************************

void Game::drawMenu()
{
	backgroundMenu.desenha(0, 0);
	guiMenu.desenha();
	SDL_ShowCursor(SDL_ENABLE);
}

void Game::drawCredits()
{
	backgroundCredits.desenha(0, 0);
	guiCredits.desenha();
	SDL_ShowCursor(SDL_ENABLE);
}

void Game::draw()
{

	// DRAWINGS
	background.desenha(0, 0);

	drawCountFont();
	guiPlaying.desenha();

	for(int i = 0; i < nGraves; i++)
	{
		graves[i].draw();
	}
    
	for(int i = 0; i < nZombies; i++)
	{
		zombies[i].draw();
	}
    if(!mouse_b)
		hammer.desenha(mouse_x - 10, mouse_y - 10);
	else
		hammer.desenha_rotacionado(mouse_x - 10, mouse_y - 10, 45); // animado

	SDL_ShowCursor(SDL_DISABLE);
}

void Game::drawGameOver()
{
	// DRAWINGS
	backgroundGameOver.desenha(0 , 0);
	guiGameOver.desenha();

	SDL_ShowCursor(SDL_ENABLE);
}

void Game::drawCountFont()
{

	switch(zombiesWhacked)
	{
	case 0:
		countFont.desenha_texto("10", 80, 34, 0, 0, 0);
		break;
	case 1:
		countFont.desenha_texto("09", 80, 34, 0, 0, 0);
		break;
	case 2:
		countFont.desenha_texto("08", 80, 34, 0, 0, 0);
		break;
	case 3:
		countFont.desenha_texto("07", 80, 34, 0, 0, 0);
		break;
	case 4:
		countFont.desenha_texto("06", 80, 34, 0, 0, 0);
		break;
	case 5:
		countFont.desenha_texto("05", 80, 34, 0, 0, 0);
		break;
	case 6:
		countFont.desenha_texto("04", 80, 34, 0, 0, 0);
		break;
	case 7:
		countFont.desenha_texto("03", 80, 34, 0, 0, 0);
		break;
	case 8:
		countFont.desenha_texto("02", 80, 34, 0, 0, 0);
		break;
	case 9:
		countFont.desenha_texto("01", 80, 34, 0, 0, 0);
		break;
	default: 
		countFont.desenha_texto("00", 80, 34, 0, 0, 0);
		break;
	}
}

void Game::finalize()
{
	egl_finalizar();
}