#pragma once
#include "Zombie.h"

class Grave
{
public:
    Grave(void);
    ~Grave(void);
    
    void initialize(float x, float y);
    void destroy();
    void draw();
    void createZombies();
	
    float getX();
    float getY();
    
	int getWidth();
	int getHeight();
	
	bool isDestroyed();

protected:
    
    imagem sprite;
    som destroyGraveSound;
	Zombie zombies[50];
    float x, y;
    float graveX, graveY;
    bool destroyed;


    
};