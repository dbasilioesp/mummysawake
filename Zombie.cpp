#include "Zombie.h"

Zombie::Zombie(void){}
Zombie::~Zombie(void){}

void Zombie::initialize(int tipo, float x_, float y_)
{
	x = x_;
	y = y_;
	isDead = false;
    zombieType = tipo;

	sprite[0].carregar(".\\sprites\\MummyOfficial1.png", 0, 0, 50, 70);
	sprite[0].carregar(".\\sprites\\MummyOfficial2.png", 0, 0, 50, 70);
	sprite[0].carregar(".\\sprites\\MummyOfficial3.png", 0, 0, 50, 70);
	sprite[0].carregar(".\\sprites\\MummyOfficial4.png", 0, 0, 50, 70);
	sprite[0].setar_tempo_animacao(60);

	sprite[1].carregar(".\\sprites\\MediumMummyOfficial1.png", 0, 0, 50, 70);
	sprite[1].carregar(".\\sprites\\MediumMummyOfficial2.png", 0, 0, 50, 70);
	sprite[1].carregar(".\\sprites\\MediumMummyOfficial3.png", 0, 0, 50, 70);
	sprite[1].carregar(".\\sprites\\MediumMummyOfficial4.png", 0, 0, 50, 70);
	sprite[1].setar_tempo_animacao(60);

	sprite[2].carregar(".\\sprites\\DifficultMummyOfficial1.png", 0, 0, 50, 70);
	sprite[2].carregar(".\\sprites\\DifficultMummyOfficial2.png", 0, 0, 50, 70);
	sprite[2].carregar(".\\sprites\\DifficultMummyOfficial3.png", 0, 0, 50, 70);
	sprite[2].carregar(".\\sprites\\DifficultMummyOfficial4.png", 0, 0, 50, 70);
	sprite[2].setar_tempo_animacao(60);

	switch(tipo)
	{
		case 0:
			health = 1;
			speed = 0.05;
			break;
		case 1:
			health = 2;
			speed = 0.07;
			break;
		case 2:
			health = 3;
			speed = 0.08;
			break;
	}
	
}

void Zombie::walk()
{
	x = x - speed;
}

void Zombie::draw()
{
	if(!isDead){
		sprite[zombieType].desenha(x, y);
	}
}

float Zombie::getX(){
	return x;
}

float Zombie::getY(){
	return y;
}

int Zombie::getWidth(){
	return sprite[zombieType].getResX();
}

int Zombie::getHeight(){
	return sprite[zombieType].getResY();
}

imagem Zombie::getSprite()
{
	return sprite[zombieType];

}

void Zombie::takeDamage()
{
	health--;

	if(health == 0)
	{
		die();
	}

}

void Zombie::die()
{
	isDead = true;
	speed = 0;
}
bool Zombie::isRIP()
{
	return isDead;
}