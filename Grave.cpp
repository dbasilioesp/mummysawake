#include "Grave.h"

Grave::Grave(void){}
Grave::~Grave(void){}

void Grave::initialize(float x_, float y_)
{
	x = x_;
	y = y_;
	destroyed = false;
	sprite.carregar(".\\sprites\\sarcophagus_closed.png");
}

void Grave::draw()
{
	if(!destroyed)
	{
		sprite.desenha(x, y);
	}

}

float Grave::getX()
{
	return x;
}
float Grave::getY()
{
	return y;
}
void Grave::destroy()
{
	destroyed = true;
}

bool Grave::isDestroyed()
{
	return destroyed;
}